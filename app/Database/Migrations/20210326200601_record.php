<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Record extends Migration {
  public function up() {
    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'personal_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'description' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->addForeignKey('personal_id', 'personal', 'id');
    $this->forge->createTable('record');
  }

  public function down() {
    $this->forge->dropTable('record');
  }
}