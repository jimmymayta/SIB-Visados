<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration {
  public function up() {
    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'user' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'password' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->createTable('users');

    $db = \Config\Database::connect();
    $sql = "INSERT INTO users (user, password, creationdate) VALUES ('admin@admin.com', '123', '2021-03-26 10:09:01')";
    $db->query($sql);
    $sql = "INSERT INTO users (user, password, creationdate) VALUES ('personal@personnal.com', '1234', '2021-03-26 10:09:01')";
    $db->query($sql);
  }

  public function down() {
    $this->forge->dropTable('users');
  }
}







