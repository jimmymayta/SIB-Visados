<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Groups extends Migration {
  public function up() {
    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'group' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'description' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->createTable('groups');

    $db = \Config\Database::connect();
    $builder = $db->table('groups');

    $data = [
      [
        'group' => 'admin',
        'creationdate'  => '2021-03-26 10:09:01'
      ],
      [
        'group' => 'personal',
        'creationdate'  => '2021-03-26 10:09:01'
      ],
    ];

    $builder->insertBatch($data);
  }

  public function down() {
    $this->forge->dropTable('groups');
  }
}











