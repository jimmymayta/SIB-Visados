<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Homepage extends Migration {
  public function up() {
    $this->db->enableForeignKeyChecks();

    $this->forge->addField([
      'id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true, 'auto_increment' => true],
      'code' => ['type' => 'VARCHAR', 'constraint' => '30'],
      'personal_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'homepage' => ['type' => 'VARCHAR', 'constraint' => '30', 'null' => true],
      'homepagegroup' => ['type' => 'INT', 'constraint' => '30', 'null' => true],
      'dataone' => ['type' => 'VARCHAR', 'constraint' => '1024', 'null' => true],
      'datatwo' => ['type' => 'VARCHAR', 'constraint' => '1024', 'null' => true],
      'datathree' => ['type' => 'VARCHAR', 'constraint' => '1024', 'null' => true],
      'image' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'imagename' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'file' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'filename' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'state_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'elimination_state_id' => ['type' => 'INT', 'constraint' => 9, 'unsigned' => true],
      'description' => ['type' => 'VARCHAR', 'constraint' => '300', 'null' => true],
      'creationdate' => ['type' => 'DATETIME', 'null' => true],
      'upgradedate' => ['type' => 'DATETIME', 'null' => true],
      'eliminationdate' => ['type' => 'DATETIME', 'null' => true]
    ]);
    $this->forge->addKey('id', true);
    $this->forge->addForeignKey('personal_id', 'personal', 'id');
    $this->forge->addForeignKey('state_id', 'state', 'id');
    $this->forge->addForeignKey('elimination_state_id', 'state', 'id');
    $this->forge->createTable('homepage');
  }

  public function down() {
    $this->forge->dropTable('homepage');
  }
}











