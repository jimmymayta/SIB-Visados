<?= $this->extend('layouts/layout') ?>
<?= $this->section('content') ?>
  <main class="form-signin">
    <form action="<?= route_to('auth') ?>" method="post">
      <?= csrf_field() ?>
      <div class="d-flex justify-content-center align-items-center">
        <a href="<?= route_to('principal') ?>">
          <img class="mb-4 mt-4" src="images/logo.png" alt="Logo" width="200" height="150">
        </a>
      </div>
      <h1 class="h3 mb-3 fw-bold text-white">Login</h1>
      <input type="email" id="inputEmail" name="user" class="form-control" placeholder="Correo electrónico" required autofocus>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Contraseña" required>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
    </form>
  </main>
<?= $this->endSection() ?>
