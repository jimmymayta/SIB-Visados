<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-icons.css">
    <title>Visado de Proyectos</title>
  </head>
  <body>
    <?= $this->renderSection('content') ?>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js" async></script>
    <script type="text/javascript" src="js/jquery-3.6.0.min.js" async></script>
    <script type="text/javascript" src="js/app.js" async></script>
  </body>
</html>
