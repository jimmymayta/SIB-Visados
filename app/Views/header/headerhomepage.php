<div class="">
  <div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 link-secondary <?= ($sel === 1) ? 'text-primary' : 'text-white' ?>" href="<?= route_to('principal') ?>">Pagina principal</a>
      <a class="p-2 link-secondary <?= ($sel === 2) ? 'text-primary' : 'text-white' ?>" href="#">Pagina principal (Con)</a>
      <a class="p-2 link-secondary <?= ($sel === 3) ? 'text-primary' : 'text-white' ?>" href="#">Pestaña 1</a>
      <a class="p-2 link-secondary <?= ($sel === 4) ? 'text-primary' : 'text-white' ?>" href="#">Pestaña 2</a>
      <a class="p-2 link-secondary <?= ($sel === 5) ? 'text-primary' : 'text-white' ?>" href="#">Pestaña 3</a>
      <a class="p-2 link-secondary <?= ($sel === 6) ? 'text-primary' : 'text-white' ?>" href="#"><?= $user ?></a>
      <a class="p-2 link-secondary <?= ($sel === 7) ? 'text-primary' : 'text-white' ?>" href="<?= route_to('logout') ?>">(Salir)</a>
    </nav>
  </div>
</div>

