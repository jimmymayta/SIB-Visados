<?= $this->extend('layouts/layout') ?>
<?= $this->section('content') ?>
  <div class="container">
    <?= $this->include('header/header') ?>

    <div class="nav-scroller py-1 mb-2">
      <nav class="nav d-flex justify-content-between">
        <a class="p-2 link-secondary text-white" href="<?= route_to('principal') ?>">Pagina principal</a>
        <a class="p-2 link-secondary text-white" href="#comunicado">Comunicado</a>
        <a class="p-2 link-secondary text-white" href="#archivos">Archivos</a>
        <a class="p-2 link-secondary text-white" href="#informacion">Más información</a>
        <a class="p-2 link-secondary text-white" href="#ayuda">Ayuda</a>
        <a class="p-2 link-secondary text-white" href="#">Travel</a>
      </nav>
    </div>
  </div>

  <main id='comunicado' class="container">
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
      <div class="col-md-12 px-0">
        <h1  class="display-4 fst-italic">
          Comunicado
        </h1>
        <p class="lead my-3"><?php
            $file = fopen('file/20210322212401', 'r') or exit('error');
            while (!feof($file)) {
              echo fgets($file).'<br>';
            }
            fclose($file);
          ?></p>
        <p class="lead mb-0">
          <a href="" class="text-white fw-bold">Continue la lectura...</a>
        </p>
      </div>
    </div>

    <div id="archivos" class="row row-cols-1 row-cols-md-3 g-4 mb-2">
      <?php foreach ($homepage as $home) : ?>
        <?php if ($home->homepagegroup == 1) : ?>
          <div class="col">
            <div class="card h-100">
              <img class="d-none d-md-block" src="<?= base_url('images/'.$home->image) ?>" class="card-img-left" alt="Image">
              <div class="card-body">
                <h5 class="card-title fw-bold"><?= $home->dataone ?></h5>
                <p class="card-text"><?= $home->datatwo ?></p>
                <p class="card-text"><small class="text-muted"><?= $home->datathree ?></small></p>
                <a href="<?= route_to('descargararchivos', $home->code) ?>" class="card-link">Descargar</a>
              </div>
            </div>
          </div>
        <?php endif ?>
      <?php endforeach ?>
    </div>

    <hr class="bg-white">

    <div id="informacion" class="row">
      <div class="col-md-8">
        <article class="blog-post">
          <?php if ($homepage) : ?>
            <h2 class="blog-post-title text-white">Más información</h2>
          <?php endif ?>

          <?php foreach ($homepage as $home) : ?>
            <?php if ($home->homepagegroup == 2) : ?>
              <h3 class="text-white"><?= $home->dataone ?></h3>
              <p class="text-white"><?= $home->datatwo ?></p>
              <p class="blog-post-meta text-white"><?= $home->datathree ?></p>
            <?php endif ?>
          <?php endforeach ?>
        </article>
      </div>

      <div id="ayuda" class="col-md-4">
        <?php foreach ($homepage as $home) : ?>
          <?php if ($home->homepagegroup == 3) : ?>
            <div class="p-4 mb-3 bg-light rounded">
              <h4 class="fst-italic"><?= $home->dataone ?></h4>
              <p class="mb-0"><?= $home->datatwo ?></p>
            </div>
          <?php endif ?>
        <?php endforeach ?>

        <div id="redessociales" class="p-4">
          <?php if ($homepage) : ?>
            <h4 class="fst-italic text-white">Archives</h4>
          <?php endif ?>
          <ol class="list-unstyled mb-0">
            <?php foreach ($homepage as $home) : ?>
              <?php if ($home->homepagegroup == 1) : ?>
                <li><a class="text-white" href="<?= route_to('descargararchivos', $home->code) ?>"><?= $home->filename ?> (<?= $home->datathree ?>) </a></li>
              <?php endif ?>
            <?php endforeach ?>
          </ol>
        </div>

        <div class="p-4">
          <?php if ($homepage) : ?>
            <h4 class="fst-italic text-white">R. Sociales</h4>
          <?php endif ?>
          <ol class="list-unstyled">
            <?php foreach ($homepage as $home) : ?>
              <?php if ($home->homepagegroup == 4) : ?>
                <li><a class="text-white" href="<?= $home->datatwo?>" target="_blank"><?= $home->dataone ?></a></li>
              <?php endif ?>
            <?php endforeach ?>
          </ol>
        </div>
      </div>
    </div>
  </main>

  <div class="container">

  <div class="p-4 mb-3 bg-light rounded">
    <?php foreach ($homepage as $home) : ?>
      <?php if ($home->homepagegroup == 5) : ?>
        <h4 class="fst-italic"><?= $home->dataone ?></h4>
        <p class="mb-0"><?= $home->datatwo ?></p>
      <?php endif ?>
    <?php endforeach ?>
  </div>

  </div>

  <footer class="blog-footer">
    <p>
      Blog template built for
      <a href="https://getbootstrap.com/">Bootstrap</a> by
      <a href="https://twitter.com/mdo">@mdo</a>.
    </p>
    <p>
      <a href="#">Back to top</a>
    </p>
  </footer>
<?= $this->endSection() ?>
