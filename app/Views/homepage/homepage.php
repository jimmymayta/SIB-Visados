<?= $this->extend('layouts/layout') ?>
<?= $this->section('content') ?>
  <div class="container">
    <?= $this->include('header/header') ?>
    <?= $this->include('header/headerhomepage') ?>

    <div class="mb-3">
      <h2 class="text-white">Comunicado</h2>
      <form action="/comunicadoedit" method="post">
        <div class="form-floating mb-1">
          <textarea class="form-control" placeholder="Leave a comment here"
          id="floatingTextarea" name="comunicado" style="height: 100px"><?php
            $file = fopen('file/20210322212401', 'r') or exit('error');
            while (!feof($file)) {
              echo fgets($file);
            }
            fclose($file);
          ?></textarea>
          <label for="floatingTextarea">Comunicado</label>
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
      </form>
    </div>

    <div class="mb-3">
      <h2 class="text-white">Archivos y consultas</h2>

      <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal" data-bs-target="#modal">+</button>

      <?= $this->include('homepage/archivosconsultas') ?>

      <div class="table-responsive">
        <table class="table table-hover align-middle bg-white table-bordered table-sm">
          <thead>
            <?php if ($homepage) : ?>
              <tr>
                <th scope="col" class="col-1">Homapage</th>
                <th scope="col" class="col-1">Grupo</th>
                <th scope="col" class="col-1">Titulo</th>
                <th scope="col" class="col-2">Descripcion</th>
                <th scope="col" class="col-1">Fecha</th>
                <th scope="col" class="col-1">Imagen</th>
                <th scope="col" class="col-1">Archivo</th>
                <th scope="col" class="col-1"></th>
              </tr>
            <?php endif ?>
          </thead>
          <tbody>
            <?php foreach ($homepage as $item) : ?>
              <tr>
                <th scope="row" class="align-top"><?= $item->homepage ?></th>
                <th scope="row" class="align-top"><?= $item->homepagegroup ?></th>
                <td class="align-top"><?= $item->dataone ?></td>
                <td class="align-top"><?= $item->datatwo ?></td>
                <td class="align-top"><?= $item->datathree ?></td>
                <td class="align-top">
                  <?php if ($item->imagename) : ?>
                    <a data-bs-toggle="modal"
                    data-bs-target="#Ima<?= $item->code ?>"
                    class="text-decoration-none text-white">
                      <button type="button" class="btn btn-secondary btn-sm bg-primary" data-bs-toggle="tooltip"
                      data-bs-placement="top" title="<?= $item->imagename ?>">
                        Ver imagen
                      </button>
                    </a>
                  <?php endif ?>
                </td>
                <td class="align-top">
                  <?php if ($item->file) : ?>
                    <a href="<?= route_to('descargararchivos', $item->code) ?>" class="text-decoration-none text-white">
                      <button type="button" class="btn btn-secondary btn-sm bg-primary" data-bs-toggle="tooltip"
                      data-bs-placement="top" title="<?= $item->filename ?>">
                        Ver archivo
                      </button>
                    </a>
                  <?php endif ?>
                </td>
                <th class="align-top">
                  <a href="<?= route_to('archivoscomunicadoeliminar', $item->code) ?>"><i class="bi bi-trash"></i></a>
                  <a href="" data-bs-toggle="modal" data-bs-target="#Mod<?= $item->code ?>"><i class="bi bi-pencil"></i></a>
                </th>
              </tr>

              <div class="modal fade" id="Ima<?= $item->code ?>" tabindex="-1"
              aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-fullscreen-sm-down">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel"><?= $item->imagename ?></h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <img src="images/<?= $item->image ?>" alt="">
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="Mod<?= $item->code ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Editar archivos o consultas</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form enctype="multipart/form-data" action="<?= route_to('archivoscomunicado') ?>" method="post">
                        <div class="mb-3">
                          <input class="form-control form-control-md" type="text"
                          placeholder="Dato 1 (Titulo)" name="dato1" value="<?= $item->dataone ?>">
                        </div>

                        <div class="mb-3">
                          <input class="form-control form-control-md" type="text"
                          placeholder="Dato 2 (Descripcion)" name="dato2" value="<?= $item->datatwo ?>">
                        </div>

                        <div class="mb-3">
                          <input class="form-control form-control-md" type="text"
                          placeholder="Dato 3 (Fecha)" name="dato3" value="<?= $item->datathree ?>">
                        </div>

                        <div class="mb-3">
                          <label class="form-label" for="customFile">Imagen</label>
                          <input type="file" class="form-control" id="customFile"
                          name="imagen">
                          <p class="fw-lighter">archivo</p>
                        </div>

                        <div class="mb-3">
                          <label class="form-label" for="customFile">Archivo</label>
                          <input type="file" class="form-control" id="customFile"
                          name="archivo">
                        </div>

                        <div class="mb-3">
                          <select class="form-select form-select-md mb-3" name="grupo" required>
                            <option value="0" selected>Grupo</option>
                            <option value="1">Grupo 1 (Archivos)</option>
                            <option value="2">Grupo 2 (Mas información)</option>
                            <option value="3">Grupo 3 (Ayuda)</option>
                            <option value="4">Grupo 4 (Redes sociales)</option>
                            <option value="5">Grupo 5 (Dirección)</option>
                          </select>
                        </div>

                        <div class="">
                          <button type="submit" class="btn bg-primary text-white">Guardar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
<?= $this->endSection() ?>
