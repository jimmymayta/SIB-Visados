<div class="modal fade" id="" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar archivos o consultas</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" action="<?= route_to('archivoscomunicado') ?>" method="post">
          <div class="mb-3">
            <input class="form-control form-control-md" type="text"
            placeholder="Dato 1 (Titulo)" name="dato1" value="<?= $item->dataone ?>">
          </div>

          <div class="mb-3">
            <input class="form-control form-control-md" type="text"
            placeholder="Dato 2 (Descripcion)" name="dato2" value="<?= $item->datatwo ?>">
          </div>

          <div class="mb-3">
            <input class="form-control form-control-md" type="text"
            placeholder="Dato 3 (Fecha)" name="dato3" value="<?= $item->datathree ?>">
          </div>

          <div class="mb-3">
            <label class="form-label" for="customFile">Imagen</label>
            <input type="file" class="form-control" id="customFile"
            name="imagen">
          </div>

          <div class="mb-3">
            <label class="form-label" for="customFile">Archivo</label>
            <input type="file" class="form-control" id="customFile"
            name="archivo">
          </div>

          <div class="mb-3">
            <select class="form-select form-select-md mb-3" name="grupo" required>
              <option value="0" selected>Grupo</option>
              <option value="1">Grupo 1 (Archivos)</option>
              <option value="2">Grupo 2 (Mas información)</option>
              <option value="3">Grupo 3 (Ayuda)</option>
              <option value="4">Grupo 4 (Redes sociales)</option>
              <option value="5">Grupo 5 (Dirección)</option>
            </select>
          </div>

          <div class="">
            <button type="submit" class="btn bg-primary text-white">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>