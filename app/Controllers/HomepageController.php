<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\I18n\Time;
use App\Libraries\App;

class HomepageController extends BaseController
{
	public function index() {
    $session = \Config\Services::session();
    if (!$session->get('user')) {
      return redirect()->to('/login');
    }

    $db = \Config\Database::connect();
    $query = $db->query('SELECT * FROM homepage WHERE elimination_state_id=2');
    $results = $query->getResult();

    $data = [
      'user' => $session->get('user'),
      'sel' => 2,
      'homepage' => $results
    ];

    return view('homepage/homepage', $data);
	}

  public function comunicadoedit() {
    $session = \Config\Services::session();
    if (!$session->get('user')) {
      return redirect()->to('/login');
    }

    $com =  $this->request->getPost('comunicado');

    $file = fopen('file/20210322212401', 'w') or exit('error');
    fwrite($file, $com.PHP_EOL);
    fclose($file);
    return redirect('home');
	}

  public function archivoscomunicado() {
    $session = \Config\Services::session();
    if (!$session->get('user')) {
      return redirect()->route('login');
    }

    $app = new App();

    $dato1 = $this->request->getPost('dato1') ? $this->request->getPost('dato1') : null;
    $dato2 = $this->request->getPost('dato2') ? $this->request->getPost('dato2') : null;
    $dato3 = $this->request->getPost('dato3') ? $this->request->getPost('dato3') : null;
    $image = $this->request->getFile('imagen');
    $file = $this->request->getFile('archivo');
    $grupo = $this->request->getPost('grupo');

    $image = $image->isValid() ? $image : false;
    $file = $file->isValid() ? $file : false;

    /*$dato1 = $this->request->getPost('dato1');
    $dato2 = $this->request->getPost('dato2');
    $dato3 = $this->request->getPost('dato3');
    $image = $this->request->getFile('imagen');
    $file = $this->request->getFile('archivo');
    $grupo = $this->request->getPost('grupo');*/

    $db = \Config\Database::connect();

    $imagerandomname = $image ? $image->getRandomName() : null;
    $filerandomname = $file ? $file->getRandomName() : null;
    $imagename = $image ? $image->getName() : null;
    $filename = $file ? $file->getName() : null;

    $data = [
      'code' => $app->code(),
      'personal_id' => $session->get('id'),
      'homepage' => 'Homepage',
      'homepagegroup'  => $grupo,
      'dataone' => $dato1,
      'datatwo' => $dato2,
      'datathree' => $dato3,
      'image' => $imagerandomname,
      'imagename' => $imagename,
      'file' => $filerandomname,
      'filename' => $filename,
      'state_id' => 1,
      'elimination_state_id' => 2
    ];
    $db->table('homepage')->insert($data);

    if ($image) {
      $image->move('images', $imagerandomname);

      \Config\Services::image()
        ->withFile('images/'.$imagerandomname)
        ->fit(300, 200, 'center')
        ->save('images/'.$imagerandomname);
    }

    if ($file) {
      $file->move('file', $filerandomname);
    }

    return redirect()->route('home');
	}

  public function archivoscomunicadoeliminar($code){
    $session = \Config\Services::session();
    if (!$session->get('user')) {
      return redirect()->route('login');
    }

    $db = \Config\Database::connect();
    $builder = $db->table('homepage');
    $data = [
      'elimination_state_id' => 1
    ];

    $builder->where('code', $code);
    $builder->update($data);

    return redirect()->route('home');
  }

  public function descargararchivos($code) {
    $db = \Config\Database::connect();

    $query = $db->query("SELECT file, filename FROM homepage WHERE code='$code'");
    $row = $query->getRow();

    return $this->response->download('file/'.$row->file, null)->setFileName($row->filename);
	}
}
