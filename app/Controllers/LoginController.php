<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class LoginController extends BaseController {
	public function login() {
    $session = \Config\Services::session();
    if ($session->get('user')) {
      return redirect('home');
    }

		return view('login/login');
	}

  public function auth() {
    $db = \Config\Database::connect();
    $session = \Config\Services::session();

    $sql = "SELECT * FROM users WHERE user = ? AND password = ?";
    $query = $db->query($sql, [
      $this->request->getPost('user'),
      $this->request->getPost('password')
    ]);

    $row = $query->getRow();

    if ($row) {
      $newdata = [
        'id' => $row->id,
        'user' => $row->user
      ];
      $session->set($newdata);

      return redirect('home');
    } else {
      return redirect('login');
    }
	}

  public function logout() {
    $session = \Config\Services::session();
    $session->destroy();
    return redirect('principal');
	}
}
