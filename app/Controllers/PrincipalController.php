<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class PrincipalController extends BaseController
{
	public function index()
	{
    $db = \Config\Database::connect();
    $query = $db->query('SELECT * FROM homepage WHERE state_id=1 AND elimination_state_id=2');
    $results = $query->getResult();

    $data = [
      'homepage' => $results
    ];

		return view('principal/principal', $data);
	}
}
